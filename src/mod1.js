import { interval } from 'rxjs'
import { tap, finalize } from 'rxjs-operators'

const sub = interval(1000).pipe(
  tap((x) => console.log('module', x)),
  finalize(() => {
    console.info('%cmodule finalize called', "color:red")
  })
).subscribe()
setTimeout(() => {
  console.log('module triggering unsubscribe')
  sub.unsubscribe()
}, 3000)
